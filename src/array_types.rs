use context::{DeviceContext};

use array::{
  Shape, RegionBuf, Region2d, Region3d,
  View, MutView, WithZeros,
  RegionView, RegionMutView,
  RegionBufView, RegionBufMutView,
  RegionView2d, RegionMutView2d,
  ArrayView2d, ArrayMutView2d,
  RegionView3d, RegionMutView3d,
  ArrayView3d, ArrayMutView3d,
};
use async::{
  AsyncContext,
  Async, SyncGuard, SyncLoad, SyncStore,
  AsyncLoad, AsyncStore, AsyncSend,
};
use cuda::runtime::{
  CudaDevice, CudaStream, CudaEvent, CudaEventStatus, CudaMemcpyKind,
  cuda_alloc_device, cuda_free_device,
  cuda_memset, //cuda_memset_async,
  cuda_memcpy_async, cuda_memcpy_peer_async,
  cuda_memcpy_2d_async,
};

use std::cmp::{min};
use std::collections::{HashMap};
use std::marker::{PhantomData};
use std::mem::{size_of};
use std::ops::{Deref, DerefMut};
use std::num::{Zero};
use std::rc::{Rc};

pub struct DeviceSyncGuard<'a> {
  sync_guard_event: Rc<CudaEvent>,
  _marker: PhantomData<&'a ()>,
}

impl<'a> DeviceSyncGuard<'a> {
  pub fn new(sync_guard_event: Rc<CudaEvent>, ctx: &DeviceContext) -> DeviceSyncGuard<'a> {
    let guard = DeviceSyncGuard{
      sync_guard_event: sync_guard_event,
      _marker: PhantomData,
    };
    guard.sync_guard_event.record(&ctx.stream);
    guard
  }

  fn sync(&self) {
    match self.sync_guard_event.query() {
      Ok(CudaEventStatus::Complete) => {}
      Ok(CudaEventStatus::NotReady) => {
        self.sync_guard_event.synchronize();
      }
      Err(e) => {
        panic!("DeviceSyncGuard failed to drop: {:?}", e);
      }
    }
  }
}

impl<'a> Drop for DeviceSyncGuard<'a> {
  fn drop(&mut self) {
    self.sync();
  }
}

impl<'a> SyncGuard<'a> for DeviceSyncGuard<'a> {}

// XXX(20151017): This is for wrapping host buffers with lazy synchronizing.
pub struct LazyHostCell<T> {
  event:  Rc<CudaEvent>,
  inner:  T,
}

impl<T> LazyHostCell<T> {
  pub fn new(inner: T) -> LazyHostCell<T> {
    LazyHostCell{
      event:  Rc::new(CudaEvent::create_with_flags(0x01 | 0x02)
        .ok().expect("LazyHostCell failed to create syncing event!")),
      inner:  inner,
    }
  }

  pub fn as_view<'a, F, U>(&'a self, mut view_f: F) -> LazyHostCellView<'a, U> where F: FnMut(&'a T) -> U {
    LazyHostCellView{
      event:  self.event.clone(),
      inner:  view_f(&self.inner),
      _marker:  PhantomData,
    }
  }

  pub fn as_mut_view<'a, F, U>(&'a mut self, mut view_f: F) -> LazyHostCellView<'a, U> where F: FnMut(&'a mut T) -> U {
    LazyHostCellView{
      event:  self.event.clone(),
      inner:  view_f(&mut self.inner),
      _marker:  PhantomData,
    }
  }
}

impl<T> Drop for LazyHostCell<T> {
  fn drop(&mut self) {
    match self.event.query() {
      Ok(CudaEventStatus::Complete) => {}
      Ok(CudaEventStatus::NotReady) => {
        self.event.synchronize();
      }
      Err(e) => {
        panic!("LazyHostCell failed to drop: {:?}", e);
      }
    }
  }
}

// XXX(20151017): This is for wrapping host buffers with lazy synchronizing.
pub struct LazyHostCellView<'a, T> where T: 'a {
  event:  Rc<CudaEvent>,
  inner:  T,
  _marker:  PhantomData<&'a ()>,
}

impl<'a, T> LazyHostCellView<'a, T> where T: 'a {
  fn sync(&self) {
    match self.event.query() {
      Ok(CudaEventStatus::Complete) => {}
      Ok(CudaEventStatus::NotReady) => {
        self.event.synchronize();
      }
      Err(e) => {
        panic!("LazyHostCell failed to sync: {:?}", e);
      }
    }
  }

  pub fn get_inner(&self, ctx: &DeviceContext) -> &T {
    self.sync();
    self.event.record(&ctx.stream);
    &self.inner
  }

  pub fn get_inner_mut(&mut self, ctx: &DeviceContext) -> &mut T {
    self.sync();
    self.event.record(&ctx.stream);
    &mut self.inner
  }
}

pub enum DeviceMem<T> where T: Copy {
  Buf(DeviceBuf<T>),
  Array2d(DeviceArray2d<T>),
  Array3d(DeviceArray3d<T>),
}

pub struct DeviceBuf<T> where T: Copy {
  region: RegionBuf<T>,
  device_idx: usize,
}

impl<T> Drop for DeviceBuf<T> where T: Copy {
  fn drop(&mut self) {
    unsafe { cuda_free_device(self.region.as_mut_ptr() as *mut u8) }
      .ok().expect("SERIOUS PROBLEM: failed to free device memory!");
  }
}

impl WithZeros<u8, usize> for DeviceBuf<u8> {
  // FIXME: should take a promise.
  fn with_zeros(length: usize) -> DeviceBuf<u8> {
    let mut buf = unsafe { DeviceBuf::new(length) };
    buf.zero_out();
    buf
  }

  // FIXME: should take a promise.
  fn zero_out(&mut self) {
    let mut buf_view = self.as_mut_view();
    let buf_size = buf_view.size();
    unsafe { cuda_memset(buf_view.as_mut_ptr() as *mut u8, 0, buf_size) }
      .ok().expect("failed to set device memory!");
  }
}

impl WithZeros<f32, usize> for DeviceBuf<f32> {
  // FIXME: should take a promise.
  fn with_zeros(length: usize) -> DeviceBuf<f32> {
    let mut buf = unsafe { DeviceBuf::new(length) };
    buf.zero_out();
    buf
  }

  // FIXME: should take a promise.
  fn zero_out(&mut self) {
    let mut buf_view = self.as_mut_view();
    let buf_size = buf_view.size();
    unsafe { cuda_memset(buf_view.as_mut_ptr() as *mut u8, 0, buf_size) }
      .ok().expect("failed to set device memory!");
  }
}

impl WithZeros<i32, usize> for DeviceBuf<i32> {
  // FIXME: should take a promise.
  fn with_zeros(length: usize) -> DeviceBuf<i32> {
    let mut buf = unsafe { DeviceBuf::new(length) };
    buf.zero_out();
    buf
  }

  // FIXME: should take a promise.
  fn zero_out(&mut self) {
    let mut buf_view = self.as_mut_view();
    let buf_size = buf_view.size();
    unsafe { cuda_memset(buf_view.as_mut_ptr() as *mut u8, 0, buf_size) }
      .ok().expect("failed to set device memory!");
  }
}

impl<T> DeviceBuf<T> where T: Copy {
  pub unsafe fn new(length: usize) -> DeviceBuf<T> {
    let device_idx = CudaDevice::get_current()
      .ok().expect("failed to get current device!");
    let ptr = unsafe { cuda_alloc_device(size_of::<T>() * length) }
      .ok().expect("failed to allocate device memory!")
      as *mut T;
    DeviceBuf{
      region: unsafe { RegionBuf::new(ptr, length) },
      device_idx: device_idx,
    }
  }

  pub unsafe fn reshape(&mut self) {
    // TODO
    unimplemented!();
  }

  pub fn len(&self) -> usize {
    self.region.len()
  }

  pub fn size(&self) -> usize {
    size_of::<T>() * self.len()
  }

  pub fn as_view<'a>(&'a self) -> DeviceBufView<'a, T> {
    let dims = self.region.len();
    DeviceBufView{
      region_view: unsafe { RegionBufView::with_ptr(
          self.region.as_ptr(), 0, dims, dims,
      ) },
      device_idx: self.device_idx,
    }
  }

  pub fn as_mut_view<'a>(&'a mut self) -> DeviceBufMutView<'a, T> {
    let dims = self.region.len();
    DeviceBufMutView{
      region_view: unsafe { RegionBufMutView::with_mut_ptr(
          self.region.as_mut_ptr(), 0, dims, dims,
      ) },
      device_idx: self.device_idx,
    }
  }

  pub fn as_view_2d<'a>(&'a self, dims: (usize, usize)) -> DeviceArrayView2d<'a, T> {
    let shape: Shape = From::from(dims);
    assert!(shape.len() <= self.region.len());
    DeviceArrayView2d{
      region_view: unsafe { RegionView2d::with_ptr(
          self.region.as_ptr(), 0, dims, dims,
      ) },
      device_idx: self.device_idx,
    }
  }

  pub fn as_mut_view_2d<'a>(&'a mut self, dims: (usize, usize)) -> DeviceArrayMutView2d<'a, T> {
    let shape: Shape = From::from(dims);
    assert!(shape.len() <= self.region.len());
    DeviceArrayMutView2d{
      region_view: unsafe { RegionMutView2d::with_mut_ptr(
          self.region.as_mut_ptr(), 0, dims, dims,
      ) },
      device_idx: self.device_idx,
    }
  }

  pub fn as_view_3d<'a>(&'a self, dims: (usize, usize, usize)) -> DeviceArrayView3d<'a, T> {
    let shape: Shape = From::from(dims);
    assert!(shape.len() <= self.region.len());
    DeviceArrayView3d{
      region_view: unsafe { RegionView3d::with_ptr(
          self.region.as_ptr(), 0, dims, dims,
      ) },
      device_idx: self.device_idx,
    }
  }

  pub fn as_mut_view_3d<'a>(&'a mut self, dims: (usize, usize, usize)) -> DeviceArrayMutView3d<'a, T> {
    let shape: Shape = From::from(dims);
    assert!(shape.len() <= self.region.len());
    DeviceArrayMutView3d{
      region_view: unsafe { RegionMutView3d::with_mut_ptr(
          self.region.as_mut_ptr(), 0, dims, dims,
      ) },
      device_idx: self.device_idx,
    }
  }
}

pub struct DeviceBufView<'a, T> where T: 'a + Copy {
  region_view: RegionBufView<'a, T>,
  device_idx: usize,
}

impl<'a, T> View<'a, T, usize> for DeviceBufView<'a, T> where T: 'a + Copy {
  unsafe fn as_ptr(&'a self) -> *const T {
    self.region_view.as_ptr()
  }

  fn get_bound(&self) -> usize {
    self.region_view.get_bound()
  }

  fn get_stride(&self) -> usize {
    self.region_view.get_stride()
  }
}

impl<'a, T> DeviceBufView<'a, T> where T: 'a + Copy {
  pub fn split_at(&self, idx: usize, length: usize) -> (DeviceBufView<'a, T>, DeviceBufView<'a, T>) {
    assert!(idx <= self.region_view.len());
    let first_bound = idx;
    let second_bound = min(length, self.region_view.len() - idx);
    (
      DeviceBufView{
        region_view: unsafe { RegionBufView::with_ptr(
            self.region_view.as_ptr(), 0, first_bound, first_bound,
        ) },
        device_idx: self.device_idx,
      },
      DeviceBufView{
        region_view: unsafe { RegionBufView::with_ptr(
            self.region_view.as_ptr(), first_bound as isize, second_bound, second_bound,
        ) },
        device_idx: self.device_idx,
      },
    )
  }

  pub fn as_view_2d(&self, dims: (usize, usize)) -> DeviceArrayView2d<'a, T> {
    let shape: Shape = From::from(dims);
    assert!(shape.len() <= self.region_view.len());
    DeviceArrayView2d{
      region_view: unsafe { RegionView2d::with_ptr(
          self.region_view.as_ptr(), 0, dims, dims,
      ) },
      device_idx: self.device_idx,
    }
  }
}

impl<'a, T> Async<'a> for DeviceBufView<'a, T> where T: 'a + Copy {
  type Ctx = DeviceContext;
}

impl<'a, T> AsyncSend<'a> for DeviceBufView<'a, T> where T: 'a + Copy {
  type Mutable = DeviceBufMutView<'a, T>;

  fn send(&'a self, other: &'a mut DeviceBufMutView<'a, T>, ctx: &DeviceContext) {
    let self_bound: Shape = From::from(self.region_view.get_bound());
    let self_stride: Shape = From::from(self.region_view.get_stride());
    let other_bound: Shape = From::from(other.region_view.get_bound());
    let other_stride: Shape = From::from(other.region_view.get_stride());
    assert_eq!(self_bound, other_bound);
    if self_bound == self_stride && other_bound == other_stride {
      // TODO(20150608): should synchronize stream?
      if self.device_idx == other.device_idx {
        unsafe { cuda_memcpy_async(
            other.region_view.as_mut_ptr() as *mut u8,
            self.region_view.as_ptr() as *const u8,
            self.region_view.size(),
            CudaMemcpyKind::DeviceToDevice,
            &ctx.stream,
        ) }.ok().expect("failed to do cuda_memcpy_async!");
      } else {
        unsafe { cuda_memcpy_peer_async(
            other.region_view.as_mut_ptr() as *mut u8,
            other.device_idx,
            self.region_view.as_ptr() as *const u8,
            self.device_idx,
            self.region_view.size(),
            &ctx.stream,
        ) }.ok().expect("failed to do cuda_memcpy_peer_async!");
      }
    } else {
      panic!("UNIMPLEMENTED: failed to .send(), mismatched shapes!");
    }
  }
}

impl<'a, T> DeviceBufView<'a, T> where T: 'a + Copy {
}

pub struct DeviceBufMutView<'a, T> where T: 'a + Copy {
  region_view: RegionBufMutView<'a, T>,
  device_idx: usize,
}

impl<'a, T> Async<'a> for DeviceBufMutView<'a, T> where T: 'a + Copy {
  type Ctx = DeviceContext;
}

impl<'a, T> MutView<'a, T, usize> for DeviceBufMutView<'a, T> where T: 'a + Copy {
  unsafe fn as_mut_ptr(&mut self) -> *mut T {
    self.region_view.as_mut_ptr()
  }

  fn get_bound(&self) -> usize {
    self.region_view.get_bound()
  }

  fn get_stride(&self) -> usize {
    self.region_view.get_stride()
  }
}

impl<'a, T> DeviceBufMutView<'a, T> where T: 'a + Copy {
  pub fn split_at(&mut self, idx: usize, length: usize) -> (DeviceBufMutView<'a, T>, DeviceBufMutView<'a, T>) {
    assert!(idx <= self.region_view.len());
    let first_bound = idx;
    let second_bound = min(length, self.region_view.len() - idx);
    (
      DeviceBufMutView{
        region_view: unsafe { RegionBufMutView::with_mut_ptr(
            self.region_view.as_mut_ptr(), 0, first_bound, first_bound,
        ) },
        device_idx: self.device_idx,
      },
      DeviceBufMutView{
        region_view: unsafe { RegionBufMutView::with_mut_ptr(
            self.region_view.as_mut_ptr(), first_bound as isize, second_bound, second_bound,
        ) },
        device_idx: self.device_idx,
      },
    )
  }

  pub fn as_mut_view_2d(&mut self, dims: (usize, usize)) -> DeviceArrayMutView2d<'a, T> {
    let shape: Shape = From::from(dims);
    assert!(shape.len() <= self.region_view.len());
    DeviceArrayMutView2d{
      region_view: unsafe { RegionMutView2d::with_mut_ptr(
          self.region_view.as_mut_ptr(), 0, dims, dims,
      ) },
      device_idx: self.device_idx,
    }
  }

  pub fn as_mut_view_3d(&mut self, dims: (usize, usize, usize)) -> DeviceArrayMutView3d<'a, T> {
    let shape: Shape = From::from(dims);
    assert!(shape.len() <= self.region_view.len());
    DeviceArrayMutView3d{
      region_view: unsafe { RegionMutView3d::with_mut_ptr(
          self.region_view.as_mut_ptr(), 0, dims, dims,
      ) },
      device_idx: self.device_idx,
    }
  }
}

pub struct DeviceArray2d<T> where T: Copy {
  region: Region2d<T>,
  device_idx: usize,
}

impl<T> Drop for DeviceArray2d<T> where T: Copy {
  fn drop(&mut self) {
    unsafe { cuda_free_device(self.region.as_mut_ptr() as *mut u8) }
      .ok().expect("SERIOUS PROBLEM: failed to free device memory!");
  }
}

impl WithZeros<f32, (usize, usize)> for DeviceArray2d<f32> {
  fn with_zeros(dims: (usize, usize)) -> DeviceArray2d<f32> {
    //let shape: Shape = From::from(dims);
    let mut arr = unsafe { DeviceArray2d::new(dims) };
    arr.zero_out();
    arr
  }

  fn zero_out(&mut self) {
    let mut arr_view = self.as_mut_view();
    let arr_size = arr_view.size();
    unsafe { cuda_memset(arr_view.as_mut_ptr() as *mut u8, 0, arr_size) }
      .ok().expect("failed to set device memory!");
  }
}

impl WithZeros<i32, (usize, usize)> for DeviceArray2d<i32> {
  fn with_zeros(dims: (usize, usize)) -> DeviceArray2d<i32> {
    //let shape: Shape = From::from(dims);
    let mut arr = unsafe { DeviceArray2d::new(dims) };
    arr.zero_out();
    arr
  }

  fn zero_out(&mut self) {
    let mut arr_view = self.as_mut_view();
    let arr_size = arr_view.size();
    unsafe { cuda_memset(arr_view.as_mut_ptr() as *mut u8, 0, arr_size) }
      .ok().expect("failed to set device memory!");
  }
}

impl<T> DeviceArray2d<T> where T: Copy {
  pub unsafe fn new(dims: (usize, usize)) -> DeviceArray2d<T> {
    let device_idx = CudaDevice::get_current()
      .ok().expect("failed to get current device!");
    let shape: Shape = From::from(dims);
    let ptr = unsafe { cuda_alloc_device(size_of::<T>() * shape.len()) }
      .ok().expect("failed to allocate device memory!")
      as *mut T;
    DeviceArray2d{
      region: Region2d::new(ptr, dims),
      device_idx: device_idx,
    }
  }

  pub fn as_view<'a>(&'a self) -> DeviceArrayView2d<'a, T> {
    DeviceArrayView2d{
      region_view: unsafe { RegionView2d::with_ptr(
          self.region.as_ptr(), 0,
          self.region.get_bound(), self.region.get_stride()) },
      device_idx: self.device_idx,
    }
  }

  pub fn as_mut_view<'a>(&'a mut self) -> DeviceArrayMutView2d<'a, T> {
    DeviceArrayMutView2d{
      region_view: unsafe { RegionMutView2d::with_mut_ptr(
          self.region.as_mut_ptr(), 0,
          self.region.get_bound(), self.region.get_stride()) },
      device_idx: self.device_idx,
    }
  }
}

pub struct DeviceArrayView2d<'a, T> where T: 'a + Copy {
  region_view: RegionView2d<'a, T>,
  device_idx: usize,
}

impl<'a, T> View<'a, T, (usize, usize)> for DeviceArrayView2d<'a, T> where T: 'a + Copy {
  unsafe fn as_ptr(&'a self) -> *const T {
    self.region_view.as_ptr()
  }

  fn get_bound(&self) -> (usize, usize) {
    self.region_view.get_bound()
  }

  fn get_stride(&self) -> (usize, usize) {
    self.region_view.get_stride()
  }

  fn len(&self) -> usize {
    self.region_view.len()
  }

  fn size(&self) -> usize {
    self.region_view.size()
  }
}

impl<'a, T> Async<'a> for DeviceArrayView2d<'a, T> where T: 'a + Copy {
  type Ctx = DeviceContext;
}

impl<'a, T> SyncStore<'a> for DeviceArrayView2d<'a, T> where T: 'a + Copy {
  type SyncMutData = ArrayMutView2d<'a, T>;
  type Guard = DeviceSyncGuard<'a>;

  fn sync_store(&'a self, other: &'a mut ArrayMutView2d<'a, T>, ctx: &DeviceContext) -> DeviceSyncGuard<'a> {
    // FIXME(20151016): the final stride component should be ignored;
    // Shape makes it hard to do that.
    /*let self_bound: Shape = From::from(self.region_view.get_bound());
    let self_stride: Shape = From::from(self.region_view.get_stride());
    let other_bound: Shape = From::from(other.get_bound());
    let other_stride: Shape = From::from(other.get_stride());*/
    let self_bound = (self.region_view.get_bound());
    let self_stride = (self.region_view.get_stride());
    let other_bound = (other.get_bound());
    let other_stride = (other.get_stride());
    assert_eq!(self_bound, other_bound);
    if self_bound.0 == self_stride.0 && other_bound.0 == other_stride.0 {
      ctx.synchronize();
      let other_size = other.size();
      unsafe { cuda_memcpy_async(
          other.as_mut_ptr() as *mut u8,
          self.region_view.as_ptr() as *const u8,
          other_size,
          CudaMemcpyKind::DeviceToHost,
          &ctx.stream,
      ) }.ok().expect("failed to do device to host cuda_memcpy_async!");
      //ctx.blocking_sync();
      DeviceSyncGuard::new(ctx.get_guard_event(), ctx)
    } else {
      panic!("UNIMPLEMENTED: failed to .sync_store(), mismatched shapes!");
    }
  }
}

impl<'a, T> AsyncStore<'a> for DeviceArrayView2d<'a, T> where T: 'a + Copy {
  type AsyncCell = LazyHostCellView<'a, ArrayMutView2d<'a, T>>;

  fn async_store(&self, other: &mut LazyHostCellView<ArrayMutView2d<'a, T>>, ctx: &DeviceContext) {
    let mut other_view = other.get_inner_mut(ctx);
    // FIXME(20151016): the final stride component should be ignored;
    // Shape makes it hard to do that.
    let self_bound = (self.region_view.get_bound());
    let self_stride = (self.region_view.get_stride());
    let other_bound = (other_view.get_bound());
    let other_stride = (other_view.get_stride());
    assert_eq!(self_bound, other_bound);
    if self_bound.0 == self_stride.0 && other_bound.0 == other_stride.0 {
      ctx.synchronize();
      let other_size = other_view.size();
      unsafe { cuda_memcpy_async(
          other_view.as_mut_ptr() as *mut u8,
          self.region_view.as_ptr() as *const u8,
          other_size,
          CudaMemcpyKind::DeviceToHost,
          &ctx.stream,
      ) }.ok().expect("failed to do device to host cuda_memcpy_async!");
    } else {
      panic!("UNIMPLEMENTED: failed to .async_store(), mismatched shapes!");
    }
  }
}

impl<'a, T> AsyncSend<'a> for DeviceArrayView2d<'a, T> where T: 'a + Copy {
  type Mutable = DeviceArrayMutView2d<'a, T>;

  fn send(&'a self, other: &'a mut DeviceArrayMutView2d<'a, T>, ctx: &DeviceContext) {
    let self_bound: Shape = From::from(self.region_view.get_bound());
    let self_stride: Shape = From::from(self.region_view.get_stride());
    let other_bound: Shape = From::from(other.region_view.get_bound());
    let other_stride: Shape = From::from(other.region_view.get_stride());
    assert_eq!(self_bound, other_bound);
    if self_bound == self_stride && other_bound == other_stride {
      // TODO(20150608): should synchronize stream?
      if self.device_idx == other.device_idx {
        unsafe { cuda_memcpy_async(
            other.region_view.as_mut_ptr() as *mut u8,
            self.region_view.as_ptr() as *const u8,
            self.region_view.size(),
            CudaMemcpyKind::DeviceToDevice,
            &ctx.stream,
        ) }.ok().expect("failed to do cuda_memcpy_async!");
      } else {
        unsafe { cuda_memcpy_peer_async(
            other.region_view.as_mut_ptr() as *mut u8,
            other.device_idx,
            self.region_view.as_ptr() as *const u8,
            self.device_idx,
            self.region_view.size(),
            &ctx.stream,
        ) }.ok().expect("failed to do cuda_memcpy_peer_async!");
      }
    } else {
      let (self_bound0, self_bound1) = self.region_view.get_bound();
      let (self_stride0, _) = self.region_view.get_stride();
      let (other_stride0, _) = other.region_view.get_stride();
      if self.device_idx == other.device_idx {
        unsafe { cuda_memcpy_2d_async(
          other.region_view.as_mut_ptr() as *mut u8,
          size_of::<T>() * other_stride0,
          self.region_view.as_ptr() as *const u8,
          size_of::<T>() * self_stride0,
          size_of::<T>() * self_bound0,
          self_bound1,
          CudaMemcpyKind::DeviceToDevice,
          &ctx.stream,
        ) }.ok().expect("failed to od cuda_memcpy_2d_async!");
      } else {
        panic!("UNIMPLEMENTED: failed to .send(), missing 2d peer impl!");
      }
    }
  }
}

impl<'a, T> DeviceArrayView2d<'a, T> where T: 'a + Copy {
  pub fn view(&'a self, lower: (usize, usize), upper: (usize, usize)) -> DeviceArrayView2d<'a, T> {
    DeviceArrayView2d{
      region_view: self.region_view.view(lower, upper),
      device_idx: self.device_idx,
    }
  }
}

pub struct DeviceArrayMutView2d<'a, T> where T: 'a + Copy {
  region_view: RegionMutView2d<'a, T>,
  device_idx: usize,
}

impl<'a, T> MutView<'a, T, (usize, usize)> for DeviceArrayMutView2d<'a, T> where T: 'a + Copy {
  unsafe fn as_mut_ptr(&mut self) -> *mut T {
    self.region_view.as_mut_ptr()
  }

  fn get_bound(&self) -> (usize, usize) {
    self.region_view.get_bound()
  }

  fn get_stride(&self) -> (usize, usize) {
    self.region_view.get_stride()
  }

  fn len(&self) -> usize {
    self.region_view.len()
  }

  fn size(&self) -> usize {
    self.region_view.size()
  }
}

impl<'a, T> Async<'a> for DeviceArrayMutView2d<'a, T> where T: 'a + Copy {
  type Ctx = DeviceContext;
}

impl<'a, T> SyncLoad<'a> for DeviceArrayMutView2d<'a, T> where T: 'a + Copy {
  //type Immutable = DeviceArrayView2d<'a, T>;
  type SyncData = ArrayView2d<'a, T>;
  type Guard = DeviceSyncGuard<'a>;

  /*fn load(&'a mut self, other: &'a DeviceArrayView2d<'a, T>, ctx: &DeviceContext) {
    let self_bound: Shape = From::from(self.region_view.get_bound());
    let self_stride: Shape = From::from(self.region_view.get_stride());
    let other_bound: Shape = From::from(other.region_view.get_bound());
    let other_stride: Shape = From::from(other.region_view.get_stride());
    assert_eq!(self_bound, other_bound);
    if self_bound == self_stride && other_bound == other_stride {
      // TODO(20150608): should synchronize stream?
      let region_size = self.region_view.size();
      unsafe { cuda_memcpy_async(
          self.region_view.as_mut_ptr() as *mut u8,
          other.region_view.as_ptr() as *const u8,
          region_size,
          CudaMemcpyKind::DeviceToDevice,
          &ctx.stream,
      ) }.ok().expect("failed to do device to device cuda_memcpy_async!");
    } else {
      panic!("UNIMPLEMENTED: failed to .load(), mismatched shapes!");
    }
  }*/

  fn sync_load(&'a mut self, other: &'a ArrayView2d<'a, T>, ctx: &DeviceContext) -> DeviceSyncGuard<'a> {
    let self_bound: Shape = From::from(self.region_view.get_bound());
    let self_stride: Shape = From::from(self.region_view.get_stride());
    let other_bound: Shape = From::from(other.get_bound());
    let other_stride: Shape = From::from(other.get_stride());
    assert_eq!(self_bound, other_bound);
    if self_bound == self_stride && other_bound == other_stride {
      let region_size = self.region_view.size();
      ctx.synchronize();
      unsafe { cuda_memcpy_async(
          self.region_view.as_mut_ptr() as *mut u8,
          other.as_ptr() as *const u8,
          region_size,
          CudaMemcpyKind::HostToDevice,
          &ctx.stream,
      ) }.ok().expect("failed to do host to device cuda_memcpy_async!");
      //ctx.blocking_sync();
      DeviceSyncGuard::new(ctx.get_guard_event(), ctx)
    } else {
      panic!("UNIMPLEMENTED: failed to .sync_load(), mismatched shapes!");
    }
  }
}

impl<'a, T> AsyncLoad<'a> for DeviceArrayMutView2d<'a, T> where T: 'a + Copy {
  type AsyncCell = LazyHostCellView<'a, ArrayView2d<'a, T>>;

  fn async_load(&mut self, other: &LazyHostCellView<ArrayView2d<'a, T>>, ctx: &DeviceContext) {
    let other_view = other.get_inner(ctx);
    let self_bound: Shape = From::from(self.region_view.get_bound());
    let self_stride: Shape = From::from(self.region_view.get_stride());
    let other_bound: Shape = From::from(other_view.get_bound());
    let other_stride: Shape = From::from(other_view.get_stride());
    assert_eq!(self_bound, other_bound);
    if self_bound == self_stride && other_bound == other_stride {
      let region_size = self.region_view.size();
      ctx.synchronize();
      unsafe { cuda_memcpy_async(
          self.region_view.as_mut_ptr() as *mut u8,
          other_view.as_ptr() as *const u8,
          region_size,
          CudaMemcpyKind::HostToDevice,
          &ctx.stream,
      ) }.ok().expect("failed to do host to device cuda_memcpy_async!");
    } else {
      panic!("UNIMPLEMENTED: failed to .async_load(), mismatched shapes!");
    }
  }
}

impl<'a, T> DeviceArrayMutView2d<'a, T> where T: 'a + Copy {
  pub fn mut_view(&'a mut self, lower: (usize, usize), upper: (usize, usize)) -> DeviceArrayMutView2d<'a, T> {
    DeviceArrayMutView2d{
      region_view: self.region_view.mut_view(lower, upper),
      device_idx: self.device_idx,
    }
  }
}

pub struct DeviceArray3d<T> where T: Copy {
  region: Region3d<T>,
  device_idx: usize,
}

impl<T> Drop for DeviceArray3d<T> where T: Copy {
  fn drop(&mut self) {
    unsafe { cuda_free_device(self.region.as_mut_ptr() as *mut u8) }
      .ok().expect("SERIOUS PROBLEM: failed to free device memory!");
  }
}

impl WithZeros<f32, (usize, usize, usize)> for DeviceArray3d<f32> {
  fn with_zeros(dims: (usize, usize, usize)) -> DeviceArray3d<f32> {
    let mut arr = unsafe { DeviceArray3d::new(dims) };
    arr.zero_out();
    arr
  }

  fn zero_out(&mut self) {
    let mut arr_view = self.as_mut_view();
    let arr_size = arr_view.size();
    unsafe { cuda_memset(arr_view.as_mut_ptr() as *mut u8, 0, arr_size) }
      .ok().expect("failed to set device memory!");
  }
}

impl<T> DeviceArray3d<T> where T: Copy {
  pub unsafe fn new(dims: (usize, usize, usize)) -> DeviceArray3d<T> {
    let device_idx = CudaDevice::get_current()
      .ok().expect("failed to get current device!");
    let shape: Shape = From::from(dims);
    let ptr = unsafe { cuda_alloc_device(size_of::<T>() * shape.len()) }
      .ok().expect("failed to allocate device memory!")
      as *mut T;
    DeviceArray3d{
      region: Region3d::new(ptr, dims),
      device_idx: device_idx,
    }
  }

  pub fn as_view<'a>(&'a self) -> DeviceArrayView3d<'a, T> {
    DeviceArrayView3d{
      region_view: unsafe { RegionView3d::with_ptr(
          self.region.as_ptr(), 0,
          self.region.get_bound(), self.region.get_stride()) },
      device_idx: self.device_idx,
    }
  }

  pub fn as_mut_view<'a>(&'a mut self) -> DeviceArrayMutView3d<'a, T> {
    DeviceArrayMutView3d{
      region_view: unsafe { RegionMutView3d::with_mut_ptr(
          self.region.as_mut_ptr(), 0,
          self.region.get_bound(), self.region.get_stride()) },
      device_idx: self.device_idx,
    }
  }
}

pub struct DeviceArrayView3d<'a, T> where T: 'a + Copy {
  region_view: RegionView3d<'a, T>,
  device_idx: usize,
}

impl<'a, T> View<'a, T, (usize, usize, usize)> for DeviceArrayView3d<'a, T> where T: 'a + Copy {
  unsafe fn as_ptr(&'a self) -> *const T {
    self.region_view.as_ptr()
  }

  fn get_bound(&self) -> (usize, usize, usize) {
    self.region_view.get_bound()
  }

  fn get_stride(&self) -> (usize, usize, usize) {
    self.region_view.get_stride()
  }
}

impl<'a, T> Async<'a> for DeviceArrayView3d<'a, T> where T: 'a + Copy {
  type Ctx = DeviceContext;
}

impl<'a, T> AsyncSend<'a> for DeviceArrayView3d<'a, T> where T: 'a + Copy {
  type Mutable = DeviceArrayMutView3d<'a, T>;

  fn send(&'a self, other: &'a mut DeviceArrayMutView3d<'a, T>, ctx: &DeviceContext) {
    let self_bound: Shape = From::from(self.region_view.get_bound());
    let self_stride: Shape = From::from(self.region_view.get_stride());
    let other_bound: Shape = From::from(other.region_view.get_bound());
    let other_stride: Shape = From::from(other.region_view.get_stride());
    assert_eq!(self_bound, other_bound);
    if self_bound == self_stride && other_bound == other_stride {
      // TODO(20150608): should synchronize stream?
      if self.device_idx == other.device_idx {
        unsafe { cuda_memcpy_async(
            other.region_view.as_mut_ptr() as *mut u8,
            self.region_view.as_ptr() as *const u8,
            self.region_view.size(),
            CudaMemcpyKind::DeviceToDevice,
            &ctx.stream,
        ) }.ok().expect("failed to do cuda_memcpy_async!");
      } else {
        unsafe { cuda_memcpy_peer_async(
            other.region_view.as_mut_ptr() as *mut u8,
            other.device_idx,
            self.region_view.as_ptr() as *const u8,
            self.device_idx,
            self.region_view.size(),
            &ctx.stream,
        ) }.ok().expect("failed to do cuda_memcpy_peer_async!");
      }
    } else {
      panic!("UNIMPLEMENTED: failed to .send(), mismatched shapes!");
    }
  }
}
pub struct DeviceArrayMutView3d<'a, T> where T: 'a + Copy {
  region_view: RegionMutView3d<'a, T>,
  device_idx: usize,
}

impl<'a, T> MutView<'a, T, (usize, usize, usize)> for DeviceArrayMutView3d<'a, T> where T: 'a + Copy {
  unsafe fn as_mut_ptr(&mut self) -> *mut T {
    self.region_view.as_mut_ptr()
  }

  fn get_bound(&self) -> (usize, usize, usize) {
    self.region_view.get_bound()
  }

  fn get_stride(&self) -> (usize, usize, usize) {
    self.region_view.get_stride()
  }
}

impl<'a, T> Async<'a> for DeviceArrayMutView3d<'a, T> where T: 'a + Copy {
  type Ctx = DeviceContext;
}

impl<'a, T> SyncLoad<'a> for DeviceArrayMutView3d<'a, T> where T: 'a + Copy {
  type SyncData = ArrayView3d<'a, T>;
  type Guard = DeviceSyncGuard<'a>;

  fn sync_load(&'a mut self, other: &'a ArrayView3d<'a, T>, ctx: &DeviceContext) -> DeviceSyncGuard<'a> {
    let self_bound: Shape = From::from(self.region_view.get_bound());
    let self_stride: Shape = From::from(self.region_view.get_stride());
    let other_bound: Shape = From::from(other.get_bound());
    let other_stride: Shape = From::from(other.get_stride());
    assert_eq!(self_bound, other_bound);
    if self_bound == self_stride && other_bound == other_stride {
      ctx.synchronize();
      //let other_size = other.size();
      let region_size = self.region_view.size();
      unsafe { cuda_memcpy_async(
          self.region_view.as_mut_ptr() as *mut u8,
          other.as_ptr() as *const u8,
          region_size,
          CudaMemcpyKind::HostToDevice,
          &ctx.stream,
      ) }.ok().expect("failed to do host to device cuda_memcpy_async!");
      //ctx.blocking_sync();
      DeviceSyncGuard::new(ctx.get_guard_event(), ctx)
    } else {
      panic!("UNIMPLEMENTED: failed to .sync_load(), mismatched shapes!");
    }
  }
}

impl<'a, T> AsyncLoad<'a> for DeviceArrayMutView3d<'a, T> where T: 'a + Copy {
  type AsyncCell = LazyHostCellView<'a, ArrayView3d<'a, T>>;

  fn async_load(&mut self, other: &LazyHostCellView<ArrayView3d<'a, T>>, ctx: &DeviceContext) {
    let other_view = other.get_inner(ctx);
    let self_bound: Shape = From::from(self.region_view.get_bound());
    let self_stride: Shape = From::from(self.region_view.get_stride());
    let other_bound: Shape = From::from(other_view.get_bound());
    let other_stride: Shape = From::from(other_view.get_stride());
    assert_eq!(self_bound, other_bound);
    if self_bound == self_stride && other_bound == other_stride {
      ctx.synchronize();
      let region_size = self.region_view.size();
      unsafe { cuda_memcpy_async(
          self.region_view.as_mut_ptr() as *mut u8,
          other_view.as_ptr() as *const u8,
          region_size,
          CudaMemcpyKind::HostToDevice,
          &ctx.stream,
      ) }.ok().expect("failed to do host to device cuda_memcpy_async!");
    } else {
      panic!("UNIMPLEMENTED: failed to .async_load(), mismatched shapes!");
    }
  }
}

impl<'a, T> DeviceArrayMutView3d<'a, T> where T: 'a + Copy {
  pub fn mut_view(&'a mut self, lower: (usize, usize, usize), upper: (usize, usize, usize)) -> DeviceArrayMutView3d<'a, T> {
    DeviceArrayMutView3d{
      region_view: self.region_view.mut_view(lower, upper),
      device_idx: self.device_idx,
    }
  }
}
