#![feature(libc)]
#![feature(optin_builtin_traits)]
//#![feature(zero_one)]

extern crate array;
extern crate async;
extern crate cuda;
extern crate cuda_blas;
extern crate cuda_dnn;
extern crate cuda_rand;
extern crate linalg;
extern crate rembrandt_kernels;

extern crate libc;
extern crate rand;

/*pub use array_rand::{RandomSample};
pub use array_types::{
  DeviceMem,
  DeviceBuf,
  DeviceArray2d,
  DeviceArray3d,
};
pub use context::{DeviceContext};*/

use async::{AsyncSend};

pub mod array_linalg;
pub mod array_num;
pub mod array_rand;
pub mod array_types;
pub mod context;
pub mod sync;

/*#[test]
fn test_buffer_loads() {
  _test_buffer_loads();
}

pub fn _test_buffer_loads() {
  //let stream = Future::new(DeviceContext::new(0));
  let stream = DeviceContext::new(0);
  let buffer: DeviceBuf<f32> = unsafe { DeviceBuf::new(1024) };
  let mut buffer2: DeviceBuf<f32> = unsafe { DeviceBuf::new(1024) };
  let mut buffer3: DeviceBuf<f32> = unsafe { DeviceBuf::new(1024) };
  {
    let view = buffer.as_view_2d((32, 32));
    let mut view2 = buffer2.as_mut_view_2d((32, 32));
    view.send(&mut view2, &stream);
  }
  {
    let view2 = buffer2.as_view_2d((32, 32));
    let mut view3 = buffer3.as_mut_view_2d((32, 32));
    view2.send(&mut view3, &stream);
  }
}*/
