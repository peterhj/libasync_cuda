use async::{AsyncContext};
use cuda::ffi::runtime::{cudaDeviceFlags};
use cuda::runtime::{CudaDevice, CudaStream, CudaEvent, CudaEventStatus};
use cuda_blas::{CublasHandle};
use cuda_dnn::{CudnnHandle};
use cuda_rand::{CurandGenerator};

use libc::{c_uint};
use rand::{Rng, thread_rng};
use std::rc::{Rc};
use std::sync::{Once, ONCE_INIT};

static GLOBAL_CONTEXT: Once = ONCE_INIT;

pub struct DeviceContext {
  pub stream: CudaStream,
  sync_event: CudaEvent,
  // FIXME(20151017): should have a pool of guard events.
  sync_guard_event: Rc<CudaEvent>,
  blocking_sync_event: CudaEvent,
  pub blas: CublasHandle,
  pub dnn: CudnnHandle,
  pub rng: CurandGenerator,
}

impl AsyncContext for DeviceContext {
  fn synchronize(&self) {
    // TODO(20151017): maybe also spin w/ exponential backoff.
    match self.sync_event.query() {
      Ok(CudaEventStatus::Complete) => {}
      Ok(CudaEventStatus::NotReady) => {
        //self.stream.synchronize()
        //  .ok().expect("DeviceContext: failed to synchronize cuda stream!");
        self.blocking_sync();
      }
      Err(e) => {
        panic!("DeviceContext: failed to query sync event: {:?}", e);
      }
    }
    self.sync_event.record(&self.stream)
      .ok().expect("DeviceContext: failed to record sync event!");
    self.stream.wait_event(&self.sync_event)
      .ok().expect("DeviceContext: failed to wait on sync event!");
  }
}

impl DeviceContext {
  pub fn new(device_idx: usize) -> DeviceContext {
    GLOBAL_CONTEXT.call_once(|| {
      /*// Set device flags once globally.
      let count = CudaDevice::count()
        .ok().expect("failed to get device count!");
      for dev_idx in (0 .. count) {
        CudaDevice::set_current(dev_idx)
          .ok().expect("failed to set current device!");
        CudaDevice::reset()
          .ok().expect("failed to reset device!");
        CudaDevice::set_flags(
            cudaDeviceFlags::ScheduleBlockingSync as c_uint |
            cudaDeviceFlags::MapHost as c_uint
        )
          .ok().expect("failed to set device flags!");
      }
      CudaDevice::set_current(0)
        .ok().expect("failed to set current device!");*/
    });
    CudaDevice::set_current(device_idx)
      .ok().expect("failed to set current device!");
    //let stream = CudaStream::default();
    let stream = CudaStream::create()
      .ok().expect("failed to create cuda stream!");
    let sync_event = CudaEvent::create_with_flags(0x02) // FIXME(20151017): cudaEventDisableTiming.
      .ok().expect("failed to create cuda event for syncing stream!");
    let sync_guard_event = Rc::new(CudaEvent::create_with_flags(0x01 | 0x02) // FIXME(20151017): cudaEventDisableTiming.
      .ok().expect("failed to create cuda event for syncing stream!"));
    let blocking_sync_event = CudaEvent::create_with_flags(0x01 | 0x02) // FIXME(20151017): cudaEventBlockingSync.
      .ok().expect("failed to create cuda event for blocking sync stream!");
    let blas = CublasHandle::create()
      .ok().expect("failed to create cublas handle!");
    blas.set_stream(&stream)
      .ok().expect("failed to set stream for cublas handle!");
    let dnn = CudnnHandle::create()
      .ok().expect("failed to create cudnn handle!");
    dnn.set_stream(&stream)
      .ok().expect("failed to set stream for cudnn handle!");
    let rng = CurandGenerator::create()
      .ok().expect("failed to create curand generator!");
    rng.set_stream(&stream)
      .ok().expect("failed to set stream for curand generator!");
    rng.set_seed(thread_rng().next_u64())
      .ok().expect("failed to set seed for curand generator!");
    rng.set_offset(0)
      .ok().expect("failed to set offset for curand generator!");
    DeviceContext{
      stream: stream,
      sync_event: sync_event,
      sync_guard_event: sync_guard_event,
      blocking_sync_event: blocking_sync_event,
      blas: blas,
      dnn: dnn,
      rng: rng,
    }
  }

  pub fn get_guard_event(&self) -> Rc<CudaEvent> {
    self.sync_guard_event.clone()
  }

  pub fn blocking_sync(&self) {
    // TODO(20151017): maybe also spin w/ exponential backoff.
    self.blocking_sync_event.record(&self.stream)
      .ok().expect("DeviceContext: failed to record sync event!");
    self.blocking_sync_event.synchronize()
      .ok().expect("DeviceContext: failed to wait on sync event!");
  }
}
