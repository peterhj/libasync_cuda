use context::{DeviceContext};

use cuda::runtime::{CudaEvent, CudaEventStatus};

use std::cell::{UnsafeCell};
use std::ops::{Deref, DerefMut};
use std::sync::{Arc, LockResult, Mutex};

pub struct DeviceMutex<T> {
  data: UnsafeCell<T>,
  mutex: Mutex<bool>,
  event: CudaEvent,
}

unsafe impl<T> Send for DeviceMutex<T> {}
unsafe impl<T> Sync for DeviceMutex<T> {}

impl<T> DeviceMutex<T> {
  pub fn new(data: T) -> DeviceMutex<T> {
    DeviceMutex{
      data: UnsafeCell::new(data),
      mutex: Mutex::new(false),
      event: CudaEvent::create().ok().expect("failed to create cuda event!"),
    }
  }

  pub fn lock<'a>(&'a self, ctx: &'a DeviceContext) -> LockResult<DeviceMutexGuard<T>> {
    loop {
      {
        let mut mutex_guard = self.mutex.lock().ok().unwrap(); // FIXME
        if !*mutex_guard {
          self.event.synchronize();
          *mutex_guard = true;
          break;
        }
      }
      // TODO: sleep or spin.
    }
    Ok(DeviceMutexGuard{
      data: &self.data,
      mutex: &self.mutex,
      event: &self.event,
      ctx: ctx,
    })
  }
}

pub struct DeviceMutexGuard<'a, T> where T: 'a {
  data: &'a UnsafeCell<T>,
  mutex: &'a Mutex<bool>,
  event: &'a CudaEvent,
  ctx: &'a DeviceContext,
}

impl<'a, T> !Send for DeviceMutexGuard<'a, T> where T: 'a {}

impl<'mutex, T> Drop for DeviceMutexGuard<'mutex, T> {
  #[inline]
  fn drop(&mut self) {
    // FIXME(20150629): poisoning.
    let mut mutex_guard = self.mutex.lock().ok().unwrap(); // FIXME
    self.event.record(&self.ctx.stream);
    *mutex_guard = false;
  }
}

impl<'mutex, T> Deref for DeviceMutexGuard<'mutex, T> {
  type Target = T;

  fn deref<'a>(&'a self) -> &'a T {
    unsafe { &*self.data.get() }
  }
}

impl<'mutex, T> DerefMut for DeviceMutexGuard<'mutex, T> {
  fn deref_mut<'a>(&'a mut self) -> &'a mut T {
    unsafe { &mut *self.data.get() }
  }
}

/*// TODO:
// When reading:
// - on guard, continue if registered events are all "read".
// - on drop, register a "read" event and submit to the cuda stream.
// When writing:
// - on guard, continue if no registered events.
// - on drop, register a "write" event and submit to the cuda stream.
pub struct DeviceRwLock<T> {
  mutex: Mutex<()>,
  //inner: UnsafeCell<DeviceRwLockInner>,
  data: UnsafeCell<T>,
}

unsafe impl<T> Send for DeviceRwLock<T> {}

impl<T> DeviceRwLock<T> {
  pub fn try_read<'a>(&self) {
    let guard = self.mutex.lock()
      .ok().expect("failed to lock mutex!");

    let mut progress = true;
    let mut inner = unsafe { &mut *self.inner.get() };

    // FIXME: figure out how this lock is going to work!

    // Query the write event if it exists. Only progress if it is completed.
    if inner.write_event.is_some() {
      match inner.write_event.as_ref().unwrap().query() {
        Ok(status) => match status {
          CudaEventStatus::Completed => {
            inner.write_event = None;
          }
          CudaEventStatus::NotReady => {
            progress = false;
          }
        },
        Err(_) => panic!("failed to query cuda event!"),
      }
    }

    // Query all read events. Clear any that have completed.
    for ev in inner.read_events.iter() {
      match ev.query() {
        Ok(status) => match status {
          CudaEventStatus::Completed => {
            // TODO: remove event from list.
          }
          CudaEventStatus::NotReady => {
            // Do nothing.
          }
        },
        Err(_) => panic!("failed to query cuda event!"),
      }
    }

    // TODO
    /*if progress {
      Some(AsyncReadGuard::new(self.inner.clone()))
    } else {
      None
    }*/
    unimplemented!();
  }

  pub fn try_write<'a>(&self) {
    // TODO
    unimplemented!();
  }
}*/

/*pub struct DeviceRwLockInner {
  key_counter: u64,
}

pub struct EventPair {
  init: CudaEvent,
  term: CudaEvent,
}

pub struct DeviceRwLockReaderGuard<'a> {
  // FIXME: events should belong to the shared internal state.
  write_init_event: CudaEvent,
  write_term_event: CudaEvent,
  read_events: HashMap<u64, EventPair>,
  inner: &'a UnsafeCell<DeviceRwLockInner>,
  promise: &'a Promise<'a, DeviceContext>,
  key: Option<u64>,
  //_marker: PhantomData<&'a ()>,
}

impl<'a> Drop for DeviceRwLockReaderGuard<'a> {
  fn drop(&mut self) {
    self.read_events[&self.key.unwrap()].term.record(&self.promise.stream) // FIXME
      .ok().expect("SERIOUS PROBLEM: failed inside reader guard deconstructor!");
  }
}

impl<'a> DeviceRwLockReaderGuard<'a> {
  pub fn try_read(&mut self) -> bool {
    // Require that the writer events are complete.
    match self.write_init_event.query() {
      Ok(status) => match status {
        CudaEventStatus::Complete => {}
        CudaEventStatus::NotReady => return false,
      },
      Err(_) => panic!("SERIOUS PROBLEM: failed inside reader guard"),
    }
    match self.write_term_event.query() {
      Ok(status) => match status {
        CudaEventStatus::Complete => {}
        CudaEventStatus::NotReady => return false,
      },
      Err(_) => panic!("SERIOUS PROBLEM: failed inside reader guard"),
    }

    // Clean up complete reader events.
    let mut clean_ev_keys: Vec<u64> = Vec::new();
    for (key, ev_pair) in self.read_events.iter() {
      match ev_pair.init.query() {
        Ok(status) => match status {
          CudaEventStatus::Complete => {}
          CudaEventStatus::NotReady => continue,
        },
        Err(_) => panic!("SERIOUS PROBLEM: failed inside reader guard"),
      }
      match ev_pair.term.query() {
        Ok(status) => match status {
          CudaEventStatus::Complete => {}
          CudaEventStatus::NotReady => continue,
        },
        Err(_) => panic!("SERIOUS PROBLEM: failed inside reader guard"),
      }
      clean_ev_keys.push(*key);
    }
    for &key in clean_ev_keys.iter() {
      self.read_events.remove(&key);
    }

    // Record a reader event and add it to the internal state.
    let init_ev = CudaEvent::create()
      .ok().expect("SERIOUS PROBLEM: failed to create event inside reader guard!");
    let term_ev = CudaEvent::create()
      .ok().expect("SERIOUS PROBLEM: failed to create event inside reader guard!");
    init_ev.record(&self.promise.stream);
    let ev_pair = EventPair{init: init_ev, term: term_ev};
    let mut inner = unsafe { &mut *self.inner.get() };
    let key = inner.key_counter;
    inner.key_counter += 1;
    self.read_events.insert(key, ev_pair); // FIXME: should not already contain key.
    self.key = Some(key);

    true
  }
}*/
