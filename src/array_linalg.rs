use array_types::{DeviceArrayView2d, DeviceArrayMutView2d};
use context::{DeviceContext};

use array::{View, MutView};
use linalg::blas::{Blas, BVector, BMatrix, Transpose};

use cuda_blas::{
  CublasPointerMode,
  CublasTranspose,
  cublas_sscal,
  cublas_saxpy,
  cublas_sgemv,
  cublas_sgemm,
};

pub trait ToCublas<T> {
  type Output;

  fn to_cublas(&self) -> Self::Output;
}

impl ToCublas<Transpose> for Transpose {
  type Output = CublasTranspose;

  fn to_cublas(&self) -> CublasTranspose {
    match self {
      &Transpose::N => CublasTranspose::N,
      &Transpose::T => CublasTranspose::T,
      &Transpose::H => CublasTranspose::H,
    }
  }
}

impl<'a> Blas<'a, f32> for DeviceArrayMutView2d<'a, f32> {
  type Ctx = DeviceContext;
}

impl<'a> BVector<'a, f32> for DeviceArrayMutView2d<'a, f32> {
  type Matrix = DeviceArrayView2d<'a, f32>;
  type Vector = DeviceArrayView2d<'a, f32>;

  // FIXME(20150615): Following should be part of BScalar.
  /*fn inner_prod(&mut self, alpha: f32, x: &DeviceArrayView2d<'a, f32>, y: &DeviceArrayView2d<'a, f32>, ctx: &DeviceContext) {
    // TODO
    unimplemented!();
  }*/

  fn row_vector_scale(&mut self, alpha: f32, ctx: &DeviceContext) {
    let (m, n) = self.get_bound();
    assert_eq!(m, 1);
    let (incx, _) = self.get_stride();
    ctx.blas.set_pointer_mode(CublasPointerMode::Host);
    unsafe { cublas_sscal(
        &ctx.blas,
        n,
        alpha,
        self.as_mut_ptr(), incx,
    ) }.ok().expect("failed to call cublas_sscal");
  }

  fn col_vector_sum(&mut self, alpha: f32, x: &DeviceArrayView2d<'a, f32>, ctx: &DeviceContext) {
    // TODO
    unimplemented!();
  }

  fn row_vector_sum(&mut self, alpha: f32, x: &DeviceArrayView2d<'a, f32>, ctx: &DeviceContext) {
    let (m, n) = self.get_bound();
    let (x_m, x_n) = x.get_bound();
    assert_eq!(n, x_n);
    assert_eq!(m, 1);
    assert_eq!(x_m, 1);
    let (incy, _) = self.get_stride();
    let (incx, _) = x.get_stride();
    ctx.blas.set_pointer_mode(CublasPointerMode::Host);
    unsafe { cublas_saxpy(
        &ctx.blas,
        n,
        alpha,
        x.as_ptr(), incx,
        self.as_mut_ptr(), incy,
    ) }.ok().expect("failed to call cublas_saxpy!");
  }

  fn scalar_vector_prod(&mut self, alpha: f32, ctx: &DeviceContext) {
    // TODO
    unimplemented!();
  }

  fn matrix_vector_prod(&mut self, alpha: f32, a: &DeviceArrayView2d<'a, f32>, trans_a: Transpose, x: &DeviceArrayView2d<'a, f32>, ctx: &DeviceContext) {
    // TODO
    unimplemented!();
  }
}

impl<'a> BMatrix<'a, f32> for DeviceArrayMutView2d<'a, f32> {
  type Matrix = DeviceArrayView2d<'a, f32>;
  type Vector = DeviceArrayView2d<'a, f32>;

  fn matrix_scale(&mut self, alpha: f32, ctx: &DeviceContext) {
    let (m, n) = self.get_bound();
    if self.get_bound() ==  self.get_stride() {
      ctx.blas.set_pointer_mode(CublasPointerMode::Host);
      unsafe { cublas_sscal(
          &ctx.blas,
          m * n,
          alpha,
          self.as_mut_ptr(), 1,
      ) }.ok().expect("failed to call cublas_sscal!");
    } else {
      panic!("strided shapes not supported yet for .matrix_scale()!");
    }
  }

  fn matrix_sum(&mut self, alpha: f32, x: &DeviceArrayView2d<'a, f32>, ctx: &DeviceContext) {
    let (m, n) = self.get_bound();
    let (x_m, x_n) = x.get_bound();
    assert_eq!(n, x_n);
    assert_eq!(m, x_m);
    if self.get_bound() ==  self.get_stride() && x.get_bound() == x.get_stride() {
      ctx.blas.set_pointer_mode(CublasPointerMode::Host);
      unsafe { cublas_saxpy(
          &ctx.blas,
          m * n,
          alpha,
          x.as_ptr(), 1,
          self.as_mut_ptr(), 1,
      ) }.ok().expect("failed to call cublas_saxpy!");
    } else {
      panic!("strided shapes not supported yet for .matrix_sum()!");
    }
  }

  fn scalar_matrix_prod(
      &mut self,
      alpha: f32,
      a: &DeviceArrayView2d<'a, f32>,
      ctx: &DeviceContext)
  {
    // TODO
    unimplemented!();
  }

  fn rank_one_update(
      &mut self,
      x: &DeviceArrayView2d<f32>,
      y: &DeviceArrayView2d<f32>,
      ctx: &DeviceContext)
  {
    // TODO
    unimplemented!();
  }

  fn hadamard_prod(&mut self, a: &Self::Matrix, ctx: &Self::Ctx) {
    // TODO
    unimplemented!();
  }

  fn matrix_prod(
      &mut self,
      alpha: f32,
      a: &DeviceArrayView2d<f32>, trans_a: Transpose,
      b: &DeviceArrayView2d<f32>, trans_b: Transpose,
      beta: f32,
      ctx: &DeviceContext)
  {
    let (m, n) = self.get_bound();
    let (a_m, a_n) = a.get_bound();
    let (b_m, b_n) = b.get_bound();
    let (at_m, at_n) = match trans_a {
      Transpose::N => (a_m, a_n),
      Transpose::T | Transpose::H => (a_n, a_m),
    };
    let (bt_m, bt_n) = match trans_b {
      Transpose::N => (b_m, b_n),
      Transpose::T | Transpose::H => (b_n, b_m),
    };
    assert_eq!(m, at_m);
    assert_eq!(n, bt_n);
    assert_eq!(at_n, bt_m);
    let k = at_n;
    let (lda, _) = a.get_stride();
    let (ldb, _) = b.get_stride();
    let (ldc, _) = self.get_stride();
    ctx.blas.set_pointer_mode(CublasPointerMode::Host);
    unsafe { cublas_sgemm(
        &ctx.blas,
        trans_a.to_cublas(), trans_b.to_cublas(),
        m, n, k,
        alpha,
        a.as_ptr(), lda,
        b.as_ptr(), ldb,
        beta,
        self.as_mut_ptr(), ldc,
    ) }.ok().expect("failed to call cublas_sgemm!");
  }
}
