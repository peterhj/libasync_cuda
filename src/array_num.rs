use array_types::{DeviceBufMutView, DeviceArrayMutView2d};
use context::{DeviceContext};

use array::{MutView};
use rembrandt_kernels::*;

pub trait DeviceNumExt<T> where T: Copy {
  type Ctx;

  fn set_constant(&mut self, constant: T, ctx: &Self::Ctx);
}

impl<'a> DeviceNumExt<f32> for DeviceBufMutView<'a, f32> {
  type Ctx = DeviceContext;

  fn set_constant(&mut self, constant: f32, ctx: &DeviceContext) {
    unsafe { rembrandt_kernel_map_set_constant_float(
      self.len() as i32,
      self.as_mut_ptr(),
      constant,
      ctx.stream.ptr,
    ) };
  }
}

impl<'a> DeviceNumExt<f32> for DeviceArrayMutView2d<'a, f32> {
  type Ctx = DeviceContext;

  fn set_constant(&mut self, constant: f32, ctx: &DeviceContext) {
    unsafe { rembrandt_kernel_map_set_constant_float(
      self.len() as i32,
      self.as_mut_ptr(),
      constant,
      ctx.stream.ptr,
    ) };
  }
}
