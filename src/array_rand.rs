use array_types::{DeviceBufMutView, DeviceArrayMutView2d};
use context::{DeviceContext};

use array::{MutView};

pub trait DeviceRandExt {
  type Ctx;

  fn sample_normal(&mut self, mean: f32, std: f32, ctx: &Self::Ctx);
  fn sample_uniform(&mut self, ctx: &Self::Ctx);
}

impl<'a> DeviceRandExt for DeviceBufMutView<'a, f32> {
  type Ctx = DeviceContext;

  fn sample_normal(&mut self, mean: f32, std: f32, ctx: &DeviceContext) {
    unsafe { ctx.rng.generate_normal(self.as_mut_ptr(), self.len(), mean, std) }
      .ok().expect("failed to sample normal random numbers!");
  }

  fn sample_uniform(&mut self, ctx: &DeviceContext) {
    unsafe { ctx.rng.generate_uniform(self.as_mut_ptr(), self.len()) }
      .ok().expect("failed to sample uniform random numbers!");
  }
}

impl<'a> DeviceRandExt for DeviceArrayMutView2d<'a, f32> {
  type Ctx = DeviceContext;

  fn sample_normal(&mut self, mean: f32, std: f32, ctx: &DeviceContext) {
    if self.get_bound() == self.get_stride() {
      unsafe { ctx.rng.generate_normal(self.as_mut_ptr(), self.len(), mean, std) }
        .ok().expect("failed to sample normal random numbers!");
    } else {
      // TODO
      unimplemented!();
    }
  }

  fn sample_uniform(&mut self, ctx: &DeviceContext) {
    if self.get_bound() == self.get_stride() {
      unsafe { ctx.rng.generate_uniform(self.as_mut_ptr(), self.len()) }
        .ok().expect("failed to sample uniform random numbers!");
    } else {
      // TODO
      unimplemented!();
    }
  }
}
